Problem Description
--------------------


I wonder what this really is... enc

''.join([chr((ord(flag[i]) << 8) + ord(flag[i + 1])) for i in range(0, len(flag), 2)])

Points
------
20

Hints
-----
    1.You may find some decoders online



My Approach
-----------

1. decode convert the characters into binary (is used an online converter )

2. Upon further analyzing the code given in the description, we can deduce that the value is equal to the characters in the file and the flag is the value of the array "flag".

    28777 = ord(flag[0]) << 8 + ord(flag[1])
    ord('p') <<8 + ord('i')
    = 112 << 8 + 105
    = 28672 + 105 = 288777

3. created a python the script and run it to decode it
(PS:I looped through all of the values in the ASCII table just in case and each number outputed two characters which formed)





