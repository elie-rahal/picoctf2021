#! usr/bin/env pyhton3

def dunc(enc_code):
	flag_list=[char for char in enc_code]
	conv_asc_dec=[]
	e=0
	for e in range(0,len(enc_code)-1):
		conv_asc_dec.append(ord(flag_list[e]))

	return conv_asc_dec

enc_code=' '   # insert the encript text here

enc_decimal=dunc(enc_code)

decoded_list=[]

for j in range(0,len(enc_decimal)):
	for i in range(126):
		if ((enc_decimal[j] - (i << 8)) <= 126 and (enc_decimal[j] - (i << 8)) >= 0):
			decoded_list.append(''.join(chr(i) + chr(enc_decimal[j]-(i << 8))))


decoded_text = "".join(decoded_list)
print("Decoded text is: "+decoded_text)


