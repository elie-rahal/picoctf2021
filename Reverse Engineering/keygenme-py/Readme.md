Problem Description
--------------------

Keygenme-trial.py

Points
------
30

Hints
-----
    (None)

My Approach
-----------

1. this file hase the extention .py so it is a python script
2. looking at the script we see this sting "picoCTF{1n_7h3_|<3y_of_" in a variabel named key_part_static1_trial indicate the start of our flag
3. so to find the flag we need to know the value of this variabel key_full_template_trial composed of key_part_static1_trial + key_part_dynamic1_trial + key_part_static2_trial we have key_part_static1_trial and key_part_static2_trial the missing one is key_part_dynamic1_trial
4.  doing down the script we find the function check_key.
5.  added the folowing script to fined the flag 

print('================ The Flag =====================\n')
Picoflag = key_part_static1_trial + str("".join([hashlib.sha256(username_trial.encode("utf-8","replace")).hexdigest()[x] for x in [4,5,3,6,2,7,1,8]])) + key_part_static2_trial
print(Picoflag)


6. run the script the flag will be shown on the top