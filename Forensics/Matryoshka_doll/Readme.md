Problem Description
--------------------

Matryoshka dolls are a set of wooden dolls of decreasing size placed one inside another. What's the final one? Image: this

Points
------

30

Hints
-----

    1.  Wait, you can hide files inside files? But how do you find them?
    2.  Make sure to submit the flag as picoCTF{XXXXX}

My Approach
-----------
1.  Hiding files inside of files really stumped at first thinking it might be steganography. trying to extract it in steghide nothing is extracted my next step was figuring it with hexedit
2.  i ususaly used Hexed.it
3.  insert the image in hexed.it buy drag and drop
4.  search for IEnd and select from Pk till the end
5.  right click and crop selected bytes save the result as any name your want as a zip file
6.  unzip the file and you see an other image
7.  repeat the step 3 till 6 till you find the flag.txt
8.  open the flag and remouve the spacese
9.  you found the flag