Problem Description
--------------------

A one-time pad is unbreakable, but can you manage to recover the flag? (Wrap with picoCTF{}) nc mercury.picoctf.net 11188 otp.py

Points
------
40

Hints
-----
    1.Maybe there's a way to make this a 2x pad.



My Approach
-----------

1. When we connect to the attached service, we first get the encrypted flag, then are able to encrypt as much data as we want
2. the service implementation, we see that it uses a XOR pad of length 50000 to encrypt the input. This should be unbreakable if it's used as a one-time-pad, but in our case the service performs a wrap-around and reuses the same pad for every 50000 characters.
3. to retrieve the XOR values used to encrypt the flag, we just need to cause a wraparound, allowing our known input to be re-encrypted with the same XOR values. Since we know the input, we can XOR it with the encrypted result to get the key. Then, it's just a matter of XOR-ing the key with the encrypted flag.
4. we can even make this a bit more efficient by re-encrypting the same encrypted flag with the XOR stream that was used to originally encrypt it. This should result in the plaintext flag.
5. run the solution.py code
6. flag found
