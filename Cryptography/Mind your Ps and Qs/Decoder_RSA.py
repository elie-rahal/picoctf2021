#! /usr/bin/env python3

from Crypto.Util.number import inverse
from Crypto.Util.number import long_to_bytes

c = 421345306292040663864066688931456845278496274597031632020995583473619804626233684
n = 631371953793368771804570727896887140714495090919073481680274581226742748040342637
e = 65537

# Compute n = pq.
# n is used as the modulus for both the public and private keys. Its length, usually expressed in bits, is the key length.
# n is released as part of the public key.
# used FactorDB.com

p = 1461849912200000206276283741896701133693
q = 431899300006243611356963607089521499045809

# Totient function totient =(p-1)*(q-1)

totient = (p-1)*(q-1)


# Determine d as d ≡ e−1 (mod λ(n)); that is, d is the modular multiplicative inverse of e modulo λ(n).

d=inverse(e,totient)


# calulating M
m= pow(c, d, n)

# convert m from long to byte and decode it to utf-8
print(long_to_bytes(m).decode("utf-8"))