Problem Description
--------------------

In RSA, a small e value can be problematic, but what about N? Can you decrypt this? values

Points
------
20

Hints
-----
    1.Bits are expensive, I used only a little bit over 100 to save money



My Approach
-----------

1. Google RSA encypton and take a look about the formulas
2. downloaded the Values e,n,c are provided so the next step will be to find p and q
3.  to compute p and q we have n the formula is n=p*q (used FactorDB.com)
4. next step is fineding the totient . the formula is totient=(p-1)*(q-1)
5. now we need to determin d as it is the modular multiplicative inverse of e modulo of totient(n)(there is a library to calculate the inverse in "Crypto.Util.number" )
6. everything set to calculate m 
7. we can understand from the Hint it is type long
8. m converting the m value from long to bytes (there is a library to calculate the long_to_bytes in "Crypto.Util.number" )
9. decode the string to utf-8 

