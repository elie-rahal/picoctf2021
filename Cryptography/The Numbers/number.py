#!usr/bin/env python3

def listToString(s): 
    str1 = "" 
    for ele in s: 
        str1 += ele  
    return str1 

letter_lists=[' ','a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

image=[16,9,3,15,3,20,6,100,20,8,5,14,21,13,2,5,18,19,13,1,19,15,14,101]

new_list=[]

for i in range(24):
	for j in range(23):	
		if int(image[i]) == j:
			new_list.append(letter_lists[int(j)])
		elif str(image[i]) == '100':
			new_list.append('{')
			break
		elif str(image[i]) == '101':
			new_list.append('}')
			break

print(listToString(new_list)) 
